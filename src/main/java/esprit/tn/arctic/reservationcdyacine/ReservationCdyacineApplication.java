package esprit.tn.arctic.reservationcdyacine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReservationCdyacineApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReservationCdyacineApplication.class, args);
	}

}
