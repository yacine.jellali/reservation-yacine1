FROM openjdk:8
VOLUME /tmp
ADD target/reservation-cdyacine-0.0.1-SNAPSHOT.jar res.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "/res.jar"]