pipeline {
    agent {
        label "master"
    }
   
    stages {
        stage("clone from git") {
            steps {
                script {
                  
                                        git branch: 'master', url: 'https://gitlab.com/yacine.jellali/reservation-yacine1.git';
                }
            }
        }

        stage("mvn clean") {
            steps {
                script {
                    bat "mvn clean"
                }
            }
        }

        stage("mvn package") {
            steps {
                script {
                    bat "mvn package -DskipTests=true"
                }
            }
        }

         stage("mvn docker image push") {
            steps {
                script {
                bat "docker login  -u yacinejellali -p costa1995"
                    bat "docker push yacinejellali/pipeline1"
                }
            }
        }
}
}